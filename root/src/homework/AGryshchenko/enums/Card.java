package homework.AGryshchenko.enums;

/**
 * Created by Программирование on 03.12.2016.
 */
public class Card {
    public enum Suit{
        HEARTS,
        DIAMONDS,
        PIKES,
        Clubs
    }

    public enum Rank{
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        TEN(10),
        JOKER(11),
        QUEEN(12),
        KING(13),
        TRUMP(14);

        int value;

        Rank(int value) {
            this.value = value;
        }

        public int getValue(){
            return value;
        }
    }

    public final Suit suit;

    public final Rank rank;
    public String shirt = "French";

    public Card(Suit suit, Rank rank) {
        this.suit = suit;
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "{'" + suit + '\'' +
                ", '" + rank + '\'' + ", " + shirt + "}";
    }
}
