package homework.AGryshchenko.inner_classes;

/**
 * Created by Программирование on 11.12.2016.
 */
public class OuterClass {

    public int x;
    private int privateVar;

    public static int staticX;

    public void methodNonStatic(){
        System.out.println("I am not static!");
    }

    public static void methodStatic(){
        System.out.println("I am static!");
    }


    public static class StaticNestedClass {
        public void workWithX(){
            methodStatic();
        }
    }


    public class InnerClass {
        public void workWithNonStatic(){
            x = 10;
            methodNonStatic();
            staticX = 1;
            methodStatic();
            privateVar = 10;
        }
    }
}