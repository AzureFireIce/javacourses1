package homework.AGryshchenko.iheritance;

/**
 * Created by Программирование on 11.12.2016.
 */
public abstract class GraphicObject implements Graphical {
    public int x,y;

    public void move(int newX, int newY){
        x += newX;
        y += newY;
        draw();
    }

    public abstract void clear();

    public abstract void resize();

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
