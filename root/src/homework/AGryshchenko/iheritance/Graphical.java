package homework.AGryshchenko.iheritance;

/**
 * Created by Программирование on 11.12.2016.
 */
public interface Graphical {
    public static final double LINE_WIDTH = 5;

    void draw();
}
