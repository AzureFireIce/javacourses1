package homework.AGryshchenko.iheritance;

/**
 * Created by Программирование on 11.12.2016.
 */
public class Circle extends GraphicObject{

    private int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public void clear() {
        System.out.println("Circle cleared");
    }

    @Override
    public void draw() {
        System.out.println("Drawing circle with center at x = " + x + ", y = " + y + " and radius: " + radius);
    }

    @Override
    public void resize() {
        System.out.println("Resizing circle");
    }
}
