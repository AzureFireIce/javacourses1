package homework.AGryshchenko.iheritance;

/**
 * Created by Программирование on 11.12.2016.
 */
public class Line extends GraphicObject {

    @Override
    public void draw() {
        System.out.println("Line cleared");
    }

    @Override
    public void clear() {
        System.out.println("Line cleared");
    }

    @Override
    public void resize() {
        System.out.println("Line resized");
    }
}
