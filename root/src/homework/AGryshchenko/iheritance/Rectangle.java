package homework.AGryshchenko.iheritance;

/**
 * Created by Программирование on 11.12.2016.
 */
public class Rectangle extends GraphicObject {

    private int height, width;

    public Rectangle(int height, int width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public void draw() {
        System.out.println("Rectangle created");
    }

    @Override
    public void clear() {
        System.out.println("Rectangle cleared");
    }

    @Override
    public void resize() {
        System.out.println("Rectangle resized");
    }
}
