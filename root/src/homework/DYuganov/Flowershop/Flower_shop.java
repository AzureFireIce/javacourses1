package homework.DYuganov.Flowershop;
import homework.DYuganov.Flowershop.FlowerInPot.PotBegunia;
import homework.DYuganov.Flowershop.Flowers.AfricanRose;
import homework.DYuganov.Flowershop.Flowers.BlueRose;
import homework.DYuganov.Flowershop.Flowers.PeaceBloom;
import homework.DYuganov.Flowershop.Flowers.Rose;
import homework.DYuganov.Flowershop.Suplement.PackagePaper;
import homework.DYuganov.Flowershop.Suplement.Parfume;
import homework.DYuganov.Flowershop.Suplement.Strip;
/**
 * Created by 123456 on 12/9/2016.
 */
public class Flower_shop {
    public static void main(String[] args) {
        Bouquet bouquet = new Bouquet();
        bouquet.addFlower(new Rose());
        bouquet.addFlower(new BlueRose());
        bouquet.addFlower(new PeaceBloom());
        bouquet.addSupplements(new Strip());
        bouquet.addSupplements(new PackagePaper());
        bouquet.addSupplements(new Parfume());
        Cart cart = new Cart();
        cart.addProduct(bouquet); // 160
        Commodity flower = new AfricanRose();
        Commodity flower1 = new AfricanRose();
        Commodity flower2 = new AfricanRose();
        cart.addProduct(flower); // 70
        cart.addProduct(flower1); // 70
        cart.addProduct(flower2); // 70
        cart.addProduct(new PotBegunia());  //50

        System.out.println("getPrice of the bouquet =" + bouquet.getPrice());
        System.out.println(bouquet.toString());
        System.out.println("Total Shoping Cart =  "+ cart.getPrice()); // should be 420 = 160 + 50 + 3*70

    }
}
