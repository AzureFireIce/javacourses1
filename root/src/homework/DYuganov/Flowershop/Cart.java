package homework.DYuganov.Flowershop;
/**
 * Created by 123456 on 12/11/2016.
 */
public class Cart  {
    public int retIndex = 0;
    public Commodity[] commodity = new Commodity[100];

    public void addProduct(Commodity product){
        commodity[retIndex++] = product;
    }

    public int getPrice(){
        int price=0;
        for(int i = 0; i < retIndex; i++){
            price += commodity[i].getPrice();
        }
        return price;
    }
}
