package homework.DYuganov.Flowershop;
/**
 * Created by 123456 on 12/11/2016.
 */
public abstract class Commodity {
    protected double price;

    public double getPrice(){
        return price;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "price=" + getPrice() +
                '}';
    }
}
