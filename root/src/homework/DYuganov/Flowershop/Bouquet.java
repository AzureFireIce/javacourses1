package homework.DYuganov.Flowershop;
import java.util.Arrays;
/**
 * Created by 123456 on 12/11/2016.
 */
public class Bouquet extends Commodity {

    @Override
    public String toString() {
        return "Bouquet{" +
                "flowers=" + flowerIndex +
                ", flowers=" + Arrays.toString(flowers) +
                ", suppments=" + suppIndex +
                ", supp=" + Arrays.toString(supp) +
                '}';
    }
    public int flowerIndex = 0;
    public Flower[] flowers = new Flower[5];
    public int suppIndex = 0;
    public Supplement[] supp = new Supplement[5];

    public void addFlower(Flower flower){
        flowers[flowerIndex++] = flower;
    }
    public void addSupplements(Supplement suppl){
        supp[suppIndex++] = suppl;
    }

    @Override
    public double getPrice(){
        int price=0;
        for(int i = 0; i < flowerIndex; i++){
              price += flowers[i].getPrice();
        }
        for(int i = 0; i < suppIndex; i++){
           Supplement suplement = supp[i];
           price += suplement.getPrice();
        }
        return price;
    }


}
