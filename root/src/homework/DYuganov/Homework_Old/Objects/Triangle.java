package Objects;

/**
 * Created by 123456 on 11/27/2016.
 */
public class Triangle extends Shape {

    public String trType;

    public Triangle(int dots, String trType) {
        super(dots);
        this.trType = trType;
    }

    public String changeType(){

        trType="Type was changed";

        return trType;
    }

}
