package Objects;

/**
 * Created by 123456 on 12/1/2016.
 */
public class Card {

    public String  rank;
    public String suit;


    @Override
    public String toString() {
        return "Card{" +
                "rank ='" + rank + '\'' +
                ", suit ='" + suit + '\'' +
                '}';
    }


    public Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public String getRank() {
        return rank;
    }

    public String getSuit() {
        return suit;
    }
}
