package Primitives;

/**
 * Created by 123456 on 11/21/2016.
 */
public class EndLessLoops {
    public static void main(String[] args) {

        // #2 Напишите цикл, который будет выполнятся бесконечно.

        int i=0;
        int j=0;

       while (i<1) {

           System.out.println("never ending While");
        }

        do {

            System.out.println("never ending do While");
        }
       while (i<5);

        for (i=0; i<1; j++){

            System.out.println("never ending For");

        }

        // end of #2

    }

}
