package Primitives;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import com.sun.org.apache.xpath.internal.SourceTree;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by 123456 on 11/27/2016.
 */
public class Calculator {


    public static void main(String[] args) {

        //3. Написать калькулятор для 5 операций над числами:
//        Сложение, вычитание, умножение, деление, остаток от деления(%).
// Ввод должен быть организован в виде "4.5 * 1.2" и вывод - "4.5 * 1.2 = 5.4"

        Scanner scanner = new Scanner(System.in);
        boolean error = false;
        double a=0, b=0, res=0;
        String decRem="", aS="", bS="";

        int i;

        do {
            try {
                  error = false;
                  System.out.print("Please enter the expression: ");
                  String expr = scanner.nextLine();

                  decRem = expr.replace(".", ""); // remove the decimal sign to move expression operator to [0] after array sort

                char[]temp = decRem.toCharArray();

                  Arrays.sort(temp);
                  i = expr.indexOf(temp[0]); // index of the expression operator in the input string always @ [0] as soon as char number of operators is is lover than char number of numbers 0-9

                  aS = expr.substring(0, i);
                  bS = expr.substring(i + 1);

                  a = Double.valueOf(aS);
                  b = Double.valueOf(bS);

              } catch (NumberFormatException e) {

                error = true;
                System.out.println("Please enter the correct expression in the format: xxx.x+xxx.x");
              }

        }while (error);

        char[]temp = decRem.toCharArray();
        Arrays.sort(temp);

        switch(temp[0]) {
            case '+':
                res = a + b;
                break;
            case '-':
                 res = a - b;
                break;
            case '*':
                 res = a * b;
                break;
            case '/':
                        // very strange but division on 0 not catched here
                try{
                 res = a / b;
                }
                catch (ArithmeticException e){

                    System.out.println("Devision on 0 ");
                }

                break;

            case '%':
                res = a % b;
                break;
        }

        res=(Math.round(res*10))/10.0;

        System.out.println(a + "  " + temp[0]+" "+ b + " = " + res);


    }

}
