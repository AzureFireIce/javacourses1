package Primitives;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

/**
 * Created by 123456 on 11/25/2016.
 */
public class RandomArray {

    public static void main(String[] args) {


    //Добрый вечер.
 //       Есть дополнительное задание. Буду рад если найдется время над ним подумать.
  //      1. Заполнить массив размером 100 случайными целыми числами от 1 до 1000.
   //     2. Отсортировать получившийся массив.


        int[] arrayUnsorted= new int[100];
        int i,j;
            for (i=0; i<100; i++)
                arrayUnsorted[i] = (int)Math.round(Math.random() * 1000); //Array with random numbers 1-1000



        System.out.println("unSorted array  " + Arrays.toString(arrayUnsorted));  // Array is not sorted


        Arrays.sort(arrayUnsorted);

        System.out.println("Sorted array (function)  " + Arrays.toString(arrayUnsorted)); // Array is sorted


        // manual sorting: algorithm Bubble sort
        int max;


        for (j=0; j<99; j++){
            for (i=0; i<99-j; i++) {

                if(arrayUnsorted[i]>arrayUnsorted[i+1]){

                    max=arrayUnsorted[i];

                    arrayUnsorted[i]=arrayUnsorted[i+1];

                    arrayUnsorted[i+1]=max;

                }

            }

        }

        System.out.println("Sorted array (manual)  " + Arrays.toString(arrayUnsorted));






    }
}
