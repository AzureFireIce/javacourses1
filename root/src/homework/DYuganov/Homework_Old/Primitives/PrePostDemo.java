package Primitives;

/**
 * Created by 123456 on 11/13/2016.
 */
public class PrePostDemo {

    public static void main(String[] args) {

        int i = 3;
        i++;
        System.out.println(i); // "4"
        ++i;
        System.out.println(i); // "5"

        System.out.println(++i); // "6"

        System.out.println(i++); // "6"  the variable was printed then increased on 1

        System.out.println(i); // "7"


        int aNumber = 3;

        if (aNumber >= 0)    // True
        {
            if (aNumber == 0) // False
                System.out.println("first string");

        }else System.out.println("second string"); // if condition was True -> else should not perform


        System.out.println("third string");

    }
}
