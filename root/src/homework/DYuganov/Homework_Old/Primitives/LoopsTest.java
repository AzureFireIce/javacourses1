package Primitives;

import java.util.Arrays;

/**
 * Created by 123456 on 11/21/2016.
 */
public class LoopsTest {

    public static void main(String[] args) {


        //  #1 Напишите цикл, который не выполнится ни разу.

        int i=2;

        while (i<1)
            i++;

        System.out.println("#1 While not performed");


        do {
            break;
        }
        while (i<5);
        System.out.println("#1 do While - break used to prevent first iteration");

        for (i=2; i==1; i++){

            System.out.println("Print this in case of the wrom condition");
        }
        System.out.println("#1 For not performed");

    //  end of #1
    // see #2 in Primitives.EndLessLoops.java

    // #3 Дополните цикл из задания 2, что бы он прервался после 10 итераций, не меняя условия завершения цикла.

        i=0;
        int j=0;

        while (i<1) {

            if(j>9)
                break;

            j++;

        }
        System.out.println("Break of 'While' in "+j+" iterations");

        j=0;
        i=0;

        do {

            if(j>9)
                break;

             j++;

        }
        while (i<5);

        System.out.println("Break of 'do While' in "+j+" iterations");
        j=0;
        for (i=0; i<1; j++){
            if(j>9)
                break;

        }

        System.out.println("Break of 'For' in "+j+" iterations");


        // end of #3

       // #4 Написать цикл который выводит подряд все числа от 1 до 100, дополнить его таким образом, что бы чётные числа
        // не выводились, не используя конструкцию if()

        for (i=1; i<101; i++){

            System.out.println("All numbers from 1 to 100="+i);

        }

        for (i=1; i<101; i+=2){

            System.out.println("Even numbers from 1 to 100="+i);
        }


        // end of #4


        // #5 Вывести на экран таблицу умножения до 20 для числа 7. ( 7х0 = 0, 7х1 = 7, 7х2 = 14, ..., 7х20 = 140)


        for (i=0; i<21; i++){

            j=7*i;

            System.out.println("7x"+i+"="+j);
        }

        // end of #5

        // #6  Создайте массив из элементов, которые составляют последовательность
        // чисел Фибоначчи, и вывести его на экран(помним фунцию(Arrays.toString());



        int[]fibo =new int[20];


        fibo[0]=0;
        fibo[1]=1;

        for (i=2; i<20; i++){

            fibo[i]=fibo[i-2]+fibo[i-1];

        }
        System.out.println("Fibonacci's numbers=" + Arrays.toString(fibo));
    }


}
