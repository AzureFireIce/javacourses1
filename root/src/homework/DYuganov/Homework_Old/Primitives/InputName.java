package Primitives;

import java.util.Scanner;

/**
 * Created by 123456 on 11/26/2016.
 */
public class InputName {

    public static void main(String[] args) {

     //   1. Написать программу, которая принимает имя, фамилию, отчество и выводит инициалы.

        Scanner scanner = new Scanner(System.in);


        System.out.print("What is your name ? :");

        String name = scanner.next();

        System.out.print("What is your middle name ? :");

        String mName = scanner.next();

        System.out.print("What is your Family name ? :");

        String fName = scanner.next();


        System.out.println(fName+" " + name.substring(0,1)+". "+ mName.substring(0,1)+".");


    }


}
